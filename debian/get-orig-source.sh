#!/bin/sh

set -ex

UPSTREAM_VERSION=$2
ORIG_ZIP=$3

WORKING_DIR=$(dirname ${ORIG_ZIP})
ORIG_TARBALL=ch5m3d_${UPSTREAM_VERSION}+dfsg.orig.tar.gz
PKGDIR=../ch5m3d-${UPSTREAM_VERSION}+dfsg

TMPDIR=$(mktemp -d --tmpdir=${WORKING_DIR})
cd ${TMPDIR}
unzip -qq ${ORIG_ZIP}
mkdir ${PKGDIR}

########################################################
# remove the uglified javascript, but keep its license #
########################################################
sed  '/function/,$ d' ch5m3d/ch5m3d.js > ch5m3d/ch5m3d.js.license_header
rm ch5m3d/ch5m3d.js
sed  '/function/,$ d' ch5m3d/qchem/ch5m3dq.js > ch5m3d/qchem/ch5m3dq.js.license_header
rm ch5m3d/qchem/ch5m3dq.js
########################################################

mv ch5m3d/* ${PKGDIR}
cd ..
tar czf ${ORIG_TARBALL} $(basename ${PKGDIR})
rm -rf $(basename ${TMPDIR})  $(basename ${ORIG_ZIP}) $(basename ${PKGDIR})
echo "Created ../${ORIG_TARBALL}"

exit 0
