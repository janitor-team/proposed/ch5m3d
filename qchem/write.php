<?php
//
// Simple PHP script to write a file
//

//
//   This values must match value in dirlist.php
//   Set parameter defining:
//      base:  base of directory path
//
$base = "data";

//
//   Get subdirectory, filename, and contents
//   from AJAX _POST.
//
$subdir = trim($_POST['path']);
$filename = trim($_POST['filename']);
$contents = $_POST['contents'];

//
//   Get username and build full file path
//
$user = $_SERVER['PHP_AUTH_USER'];
$path = "$base/$user";
if (strlen($subdir) > 2)
  $path = "$path/$subdir";

//
//   File written to new subdirectory based on filename
//   Build new path and full filename
//
$path = "$path/$filename";
$suffix = ".inp";
$fullname = "$path/$filename$suffix";
echo "File = [$filename]  (Full name = $fullname)\n";

//
//   Create new subdirectory
//
mkdir($path, 0755);

//
//   Open file handle for new file and write contents to disk
//
$fh = fopen($fullname,'w') or die("Unable to open file $fullname");
fwrite($fh, $contents);
fclose($fh);

// Start batch job
system("cd $path; /usr/local/bin/subgms $filename batch 4");
echo "Calculation should be submitted to batch queue.\n";
//echo "$filename$suffix file created, but calculation NOT actually submitted. See write.php.\n";
?>
