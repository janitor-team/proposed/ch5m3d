<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
<? require_once "auth.php" ?>
<link rel="stylesheet" type="text/css" href="ch5m3dq.css" />
<script type="text/javascript" src="ch5m3dq.js"></script>
<title>CH5M3D</title>
</head>

<body  onload="initialize()">


<p class="QCHeader">
  CH<sub>5</sub>M<sub>3D</sub>
  </p>
<ul id="Toolbar">

  <li><a href="../index.html">CH5M3D Home</a><ul>
    </ul></li>

  <li>Documentation<ul>
    <li><a href="../doc/introduction.html">Introduction</a></li>
    <li><a href="../doc/installation.html">Installation</a></li>
    <li><a href="../doc/browser.html">Web Browsers</a></li>
    <li><a href="../doc/view.html">User Interface</a></li>
    <li><a href="../doc/keyboard.html">Keyboard/Mouse</a></li>
    <li><a href="../doc/drawing.html">Drawing</a></li>
    <li><a href="../doc/fileformat.html">File Format</a></li>
    <li><a href="../documentation.pdf">PDF Manual</a></li>
    </ul></li>
  <li>Variations<ul>
    <li><a href="../doc/description.html">Description</a></li>
    <li><a href="../variations/serverView.html">Pre-Load</a></li>
    <li><a href="../variations/chooser.html">Chooser</a></li>
    <li><a href="../variations/gallery.html">Gallery</a></li>
    <li><a href="../variations/viewer300.html">Viewer (only)</a></li>
    <li><a href="../variations/viewer2x.html">View 2 Windows</a></li>
    <li><a href="../variations/two.html">Two Windows</a></li>
    <li><a href="../variations/api.html">Javascript</a></li>
    <li><a href="qchem.php">Quantum Interface</a></li>
    </ul></li>
  <li>Information<ul>
    <li><a href="../doc/about.html">About</a></li>
    <li><a href="http://sourceforge.net/projects/ch5m3d/">Project Homepage</a></li>
    <li><a href="../doc/apidoc.html">Library API Info</a></li>
    <li><a href="https://www.gnu.org/licenses/gpl.html">GNU License</a></li>
    </ul></li>
  <li class="UserName">Welcome <? echo $_SERVER['PHP_AUTH_USER'] ?></li>
  </ul>



<p class="fleft"><strong>Local files:</strong>
  <input type="file" id="molfile"
         title="Click Browse to load a file from your computer." />
  </p>
<p class="fleft"><strong>Server files:</strong>
  <input type="button" id="ServerBtn" onclick="toggleDirWin()" value="List Server files" />
  </p>

<div id="delDiv" class="HiddenDiv">
  <br />Permanently delete files:</div>

<div id="dirDiv" class="HiddenDiv">
  <br /><strong>Server Files</strong><div class="dirWindow" id="dirWindow"></div>
  </div>

<p class="clear">&nbsp;</p>

<p class="clear">&nbsp;</p>
<p class="drawWin">
  <canvas id="molcanvas" width="500" height="500"></canvas>
</p>

<p>
  <input type='button' onclick="viewmode()" value="View Mode" class="modeTab" id="modeView" />
  <input type='button' onclick="drawmode()" value="Draw Mode" class="modeTab" id="modeDraw" />
  </p>

<div id="viewDiv" class="viewDiv">
  <p><strong>View Mode</strong></p>
  <div id="formula"><p>Formula = </p></div>
  <p><input type='button' onclick="showCoord(1)" value="Show coordinates"  class="Button"  id="coord" /></p>
  <p>
    <input type='button' class="Button" id="LabelButton" onclick="showLabels()" value="Labels" />
    <input type='button' class="Button" id="ChargeButton" onclick="simpleQ()" value="Charges" />
    </p>
  <p><input class="Button" type='button' onclick="resetView()" value="Reset View" /></p>
  <p><input type='button' class="Button" onclick="showQCEdit()" value="Create Calculation Input" /></p>
  <!--
  <p><input type='button' class="Button" onclick="peptideInfo()" value="Peptide Information" /></p>
  -->
  <p><input type='button' class="Button" onclick="showQueue()" value="Show Batch Queue" /></p>
  </div>

<div id="drawDiv" class="drawDiv">
  <p><strong>Current Element:</strong></p>
  <table>
  <tr>
    <td class="main" id="mH"  onclick="pickElem('H');">H</td>
    <td class="Row"  id="Row1" colspan="12">&nbsp;</td>
    <td class="MOff" id="MOff" colspan="2" onclick="pickElem('MOff');">Organic</td>
    <td class="MOn"  id="MOn"  colspan="2" onclick="pickElem('MOn');">Metals</td>
    <td class="metl" id="pHe" onclick="pickElem('He');">He</td>
    </tr>
  <tr>
    <td class="metl" id="pLi" onclick="pickElem('Li');">Li</td>
    <td class="metl" id="pBe" onclick="pickElem('Be');">Be</td>
    <td class="Row"  id="Row2" colspan="10">&nbsp;</td>
    <td class="main" id="mB"  onclick="pickElem('B');">B</td>
    <td class="main" id="mC"  onclick="pickElem('C');">C</td>
    <td class="main" id="mN"  onclick="pickElem('N');">N</td>
    <td class="main" id="mO"  onclick="pickElem('O');">O</td>
    <td class="main" id="mF"  onclick="pickElem('F');">F</td>
    <td class="metl" id="pNe" onclick="pickElem('Ne');">Ne</td>
    </tr>
  <tr>
    <td class="metl" id="pNa" onclick="pickElem('Na');">Na</td>
    <td class="metl" id="pMg" onclick="pickElem('Mg');">Mg</td>
    <td class="Row"  id="Row3" colspan="10">&nbsp;</td>
    <td class="main" id="mAl" onclick="pickElem('Al')">Al</td>
    <td class="main" id="mSi" onclick="pickElem('Si')">Si</td>
    <td class="main" id="mP"  onclick="pickElem('P')">P</td>
    <td class="main" id="mS"  onclick="pickElem('S')">S</td>
    <td class="main" id="mCl" onclick="pickElem('Cl')">Cl</td>
    <td class="metl" id="pAr" onclick="pickElem('Ar')">Ar</td>
    </tr>
  <tr>
    <td class="metl" id="pK"  onclick="pickElem('K')">K</td>
    <td class="metl" id="pCa" onclick="pickElem('Ca')">Ca</td>
    <td class="metl" id="pSc" onclick="pickElem('Sc')">Sc</td>
    <td class="metl" id="pTi" onclick="pickElem('Ti')">Ti</td>
    <td class="metl" id="pV"  onclick="pickElem('V')">V</td>
    <td class="metl" id="pCr" onclick="pickElem('Cr')">Cr</td>
    <td class="metl" id="pMn" onclick="pickElem('Mn')">Mn</td>
    <td class="metl" id="pFe" onclick="pickElem('Fe')">Fe</td>
    <td class="metl" id="pCo" onclick="pickElem('Co')">Co</td>
    <td class="metl" id="pNi" onclick="pickElem('Ni')">Ni</td>
    <td class="metl" id="pCu" onclick="pickElem('Cu')">Cu</td>
    <td class="metl" id="pZn" onclick="pickElem('Zn')">Zn</td>
    <td class="main" id="mGa" onclick="pickElem('Ga')">Ga</td>
    <td class="main" id="mGe" onclick="pickElem('Ge')">Ge</td>
    <td class="main" id="mAs" onclick="pickElem('As')">As</td>
    <td class="main" id="mSe" onclick="pickElem('Se')">Se</td>
    <td class="main" id="mBr" onclick="pickElem('Br')">Br</td>
    <td class="metl" id="pKr" onclick="pickElem('Kr')">Kr</td>
    </tr>
  <tr>
    <td class="metl" id="pRb" onclick="pickElem('Rb')">Rb</td>
    <td class="metl" id="pSr" onclick="pickElem('Sr')">Sr</td>
    <td class="metl" id="pY"  onclick="pickElem('Y')">Y</td>
    <td class="metl" id="pZr" onclick="pickElem('Zr')">Zr</td>
    <td class="metl" id="pNb" onclick="pickElem('Nb')">Nb</td>
    <td class="metl" id="pMo" onclick="pickElem('Mo')">Mo</td>
    <td class="metl" id="pTc" onclick="pickElem('Tc')">Tc</td>
    <td class="metl" id="pRu" onclick="pickElem('Ru')">Ru</td>
    <td class="metl" id="pRh" onclick="pickElem('Rh')">Rh</td>
    <td class="metl" id="pPd" onclick="pickElem('Pd')">Pd</td>
    <td class="metl" id="pAg" onclick="pickElem('Ag')">Ag</td>
    <td class="metl" id="pCd" onclick="pickElem('Cd')">Cd</td>
    <td class="main" id="mIn" onclick="pickElem('In')">In</td>
    <td class="main" id="mSn" onclick="pickElem('Sn')">Sn</td>
    <td class="main" id="mSb" onclick="pickElem('Sb')">Sb</td>
    <td class="main" id="mTe" onclick="pickElem('Te')">Te</td>
    <td class="main" id="mI"  onclick="pickElem('I')">I</td>
    <td class="metl" id="pXe" onclick="pickElem('Xe')">Xe</td>
    </tr>
  <tr>
    <td class="metl" id="pCs" onclick="pickElem('Cs')">Cs</td>
    <td class="metl" id="pBa" onclick="pickElem('Ba')">Ba</td>
    <td class="metl" id="pLa" onclick="pickElem('La')">La</td>
    <td class="metl" id="pHf" onclick="pickElem('Hf')">Hf</td>
    <td class="metl" id="pTa" onclick="pickElem('Ta')">Ta</td>
    <td class="metl" id="pW"  onclick="pickElem('W')">W</td>
    <td class="metl" id="pRe" onclick="pickElem('Re')">Re</td>
    <td class="metl" id="pOs" onclick="pickElem('Os')">Os</td>
    <td class="metl" id="pIr" onclick="pickElem('Ir')">Ir</td>
    <td class="metl" id="pPt" onclick="pickElem('Pt')">Pt</td>
    <td class="metl" id="pAu" onclick="pickElem('Au')">Au</td>
    <td class="metl" id="pHg" onclick="pickElem('Hg')">Hg</td>
    <td class="main" id="mTl" onclick="pickElem('Tl')">Tl</td>
    <td class="main" id="mPb" onclick="pickElem('Pb')">Pb</td>
    <td class="main" id="mBi" onclick="pickElem('Bi')">Bi</td>
    <td class="main" id="mPo" onclick="pickElem('Po')">Po</td>
    <td class="main" id="mAt" onclick="pickElem('At')">At</td>
    <td class="metl" id="pRn" onclick="pickElem('Rn')">Rn</td>
  </tr>
  <tr>
    <td class="metl" id="pFr"  onclick="pickElem('Fr')">Fr</td>
    <td class="metl" id="pRa"  onclick="pickElem('Ra')">Ra</td>
    <td class="metl" id="pAc"  onclick="pickElem('Ac')">Ac</td>
    <td class="metl" id="pRf"  onclick="pickElem('Rf')">Rf</td>
    <td class="metl" id="pDb"  onclick="pickElem('Db')">Db</td>
    <td class="metl" id="pSg"  onclick="pickElem('Sg')">Sg</td>
    <td class="metl" id="pBh"  onclick="pickElem('Bh')">Bh</td>
    <td class="metl" id="pHs"  onclick="pickElem('Hs')">Hs</td>
    <td class="metl" id="pMt"  onclick="pickElem('Mt')">Mt</td>
    <td class="metl" id="pDs"  onclick="pickElem('Ds')">Ds</td>
    <td class="metl" id="pRg"  onclick="pickElem('Rg')">Rg</td>
    <td class="metl" id="pCn"  onclick="pickElem('Cn')">Cn</td>
    <td class="metl" id="pUut" onclick="pickElem('Uut')">Uut</td>
    <td class="metl" id="pFl"  onclick="pickElem('Fl')">Fl</td>
    <td class="metl" id="pUup" onclick="pickElem('Uup')">Uup</td>
    <td class="metl" id="pLv"  onclick="pickElem('Lv')">Lv</td>
    <td class="metl" id="pUus" onclick="pickElem('Uus')">Uus</td>
    <td class="metl" id="pUuo" onclick="pickElem('Uuo')">Uuo</td>
  </tr>
  <tr>
    <td class="Row"  id="RowLa" colspan="2">&nbsp;</td>
    <td class="metl" id="pCe" onclick="pickElem('Ce')">Ce</td>
    <td class="metl" id="pPr" onclick="pickElem('Pr')">Pr</td>
    <td class="metl" id="pNd" onclick="pickElem('Nd')">Nd</td>
    <td class="metl" id="pPm" onclick="pickElem('Pm')">Pm</td>
    <td class="metl" id="pSm" onclick="pickElem('Sm')">Sm</td>
    <td class="metl" id="pEu" onclick="pickElem('Eu')">Eu</td>
    <td class="metl" id="pGd" onclick="pickElem('Gd')">Gd</td>
    <td class="metl" id="pTb" onclick="pickElem('Tb')">Tb</td>
    <td class="metl" id="pDy" onclick="pickElem('Dy')">Dy</td>
    <td class="metl" id="pHo" onclick="pickElem('Ho')">Ho</td>
    <td class="metl" id="pEr" onclick="pickElem('Er')">Er</td>
    <td class="metl" id="pTm" onclick="pickElem('Tm')">Tm</td>
    <td class="metl" id="pYb" onclick="pickElem('Yb')">Yb</td>
    <td class="metl" id="pLu" onclick="pickElem('Lu')">Lu</td>
    <td class="Row"  id="RowLa2" colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td class="Row"  id="RowAc" colspan="2">&nbsp;</td>
    <td class="metl" id="pTh" onclick="pickElem('Th')">Th</td>
    <td class="metl" id="pPa" onclick="pickElem('Pa')">Pa</td>
    <td class="metl" id="pU"  onclick="pickElem('U')" >U</td>
    <td class="metl" id="pNp" onclick="pickElem('Np')">Np</td>
    <td class="metl" id="pPu" onclick="pickElem('Pu')">Pu</td>
    <td class="metl" id="pAm" onclick="pickElem('Am')">Am</td>
    <td class="metl" id="pCm" onclick="pickElem('Cm')">Cm</td>
    <td class="metl" id="pBk" onclick="pickElem('Bk')">Bk</td>
    <td class="metl" id="pCf" onclick="pickElem('Cf')">Cf</td>
    <td class="metl" id="pEs" onclick="pickElem('Es')">Es</td>
    <td class="metl" id="pFm" onclick="pickElem('Fm')">Fm</td>
    <td class="metl" id="pMd" onclick="pickElem('Md')">Md</td>
    <td class="metl" id="pNo" onclick="pickElem('No')">No</td>
    <td class="metl" id="pLr" onclick="pickElem('Lr')">Lr</td>
    <td class="Row"  id="RowAc2" colspan="2">&nbsp;</td>
  </tr>
  </table>

  <p><strong>Hybridization:</strong></p>
  <p> &nbsp; &nbsp;
    <input type='button' class="Button" id="cld1" onclick="pickClouds('1')" value="s"   />
    <input type='button' class="Button" id="cld2" onclick="pickClouds('2')" value="sp"  />
    <input type='button' class="Button" id="cld3" onclick="pickClouds('3')" value="sp2" />
    <input type='button' class="Button" id="cld4" onclick="pickClouds('4')" value="sp3" />
    </p>
  <p>
    <input type='button' class="Button" id="delbtn"  onclick="pickElem('Delete')" value="Delete Atom" />
    <input type='button' class="Button" id="delbond" onclick="pickElem('DelBond')" value="Delete Bond" />
    </p>
  <p>
    <input type='button' class="Button" id="rotbond" onclick="pickElem('RotateBond')" value="Rotate Bond" />
    <input type='button' class="Button" id="undo"    onclick="Undo('pop')" value="Undo" />
    <input type='button' class="Button" id="redo"    onclick="Undo('redo')" value="Redo" />
    </p>
  <p>
    <input type='button' class="Button" id="OptButton" onclick="mechanics()" value="Optimize Structure" />
    </p>
  </div>

<p class="clear">&nbsp;</p>

<!-- Create form to collect Calculation options -->
<div id="paramDiv" class="HiddenDiv">
<h3>Parameters for quantum mechanical calculation</h3>
<div id="User" class="User">

  <label for="Name">Name for input file:</label>
  <input type="text" id="Name" maxlength="15" class="radio" />
  <br />

  <label for="Comment">Comment:</label>
  <input type="text" id="Comment" maxlength="80" class="radio" />
  <br />

  <label for="Charge">Charge:</label>
  <input type="number" id="Charge" class="radio"
         min="-10" max="10" step="1" value="0" maxlength="3" />
  <br />

  <label for="SelectType">Type of Calculation:</label>
    <form name="SelectType" class="radio">
      <input type="radio" name="Type" value="ENERGY" />Single-Point Energy &nbsp; &nbsp; &nbsp; 
      <input type="radio" name="Type" value="OPTIMIZE" />Geometry Optimization &nbsp; &nbsp; &nbsp;
      <input type="radio" name="Type" value="FULL" checked="checked" />Optimization and Frequency
      </form>
  <br />

  <label for="SelectBasis">Basis Set:</label>
    <form name="SelectBasis" class="radio">
      <input type="radio" name="Basis" value="STO-3G" />STO-3G &nbsp; &nbsp; &nbsp; 
      <input type="radio" name="Basis" value="3-21G" />3-21G &nbsp; &nbsp; &nbsp;
      <input type="radio" name="Basis" value="6-31G*" checked="checked" />6-31G* &nbsp; &nbsp; &nbsp; 
      <input type="radio" name="Basis" value="6-311G**" />6-311G**
      </form>
  <br />

  <label for="SelectDFT">DFT Functional:</label>
    <form name="SelectDFT" class="radio">
      <input type="radio" name="DFT" value="NONE" />None &nbsp; &nbsp; &nbsp;
      <input type="radio" name="DFT" value="B3LYP" checked="checked" />B3LYP &nbsp; &nbsp; &nbsp;
      <input type="radio" name="DFT" value="B97-D" />B97-D &nbsp; &nbsp; &nbsp;
      <input type="radio" name="DFT" value="PBE0" />PBE0 &nbsp; &nbsp; &nbsp;
      <input type="radio" name="DFT" value="M11" />M11 &nbsp; &nbsp; &nbsp;
      <input type="radio" name="DFT" value="wB97X-D" />wB97X-D &nbsp; &nbsp; &nbsp;
      </form>
  <br />

  <label for="SelectPCM">PCM Solvent:</label>
    <form name="SelectPCM">
      <input type="radio" name="PCM" value="None" checked="checked" />None &nbsp; &nbsp; &nbsp;
      <input type="radio" name="PCM" value="H2O" />Water &nbsp; &nbsp; &nbsp;
      <input type="radio" name="PCM" value="CH3OH" />Methanol &nbsp; &nbsp; &nbsp;
      <input type="radio" name="PCM" value="C2H5OH" />Ethanol &nbsp; &nbsp; &nbsp;
      <input type="radio" name="PCM" value="ACETONE" />Acetone &nbsp; &nbsp; &nbsp;
      <input type="radio" name="PCM" value="THF" />THF &nbsp; &nbsp; &nbsp;
      </form>
  <br />
  </div>

<p>
  <input type='button' onclick='createQC()' value='Create data file' />
  <input type='button' onclick='hideQCEdit()' value='Cancel' />
  </p>
</div>

<div id="submitDiv" class="HiddenDiv">
<p class="instruct">You may edit the data in the <strong>Information</strong> box below.
  When finished, submit the job by pressing the [Start Calculation] button.<br />
  <input type='button' onclick='writeQC()' value='Start Calculation' />
  <input type='button' onclick='hideQCSubmit()' value='Cancel' />
  </p>
</div>

<div id="bondMatrix" class="bondMatrix"></div>

<p><strong>Information</strong></br><textarea id="information" rows="10" cols="80"></textarea></p>

<div class="Footer" id="Footer">
  The chem3d.js library copyright © 2013 by Clarke Earley<br />
  and is distributed under the terms of the
  <a href="http://www.gnu.org/licenses/gpl.html">GNU General Public License</a>.
  </div>

</body>
</html>
