<?php
//
//   Simple PHP script to delete file or directory
//


//
//   Get parameters
//
$filename = $_POST['filename'];

//
//   Attempt to delete requested file
//
echo "Attempting to delete file $filename\n";
if ( unlink($filename) === false ) {
  echo "Error deleting file.\n";
  return;
  }
echo "File successfully deleted\n";

//
//   Remove system files in current directory
//
$pos = strrpos($filename,".");
$logfile = substr($filename,0,$pos).".syslog";
echo "log file = [$logfile].\n";
if ( unlink($logfile) == true ) {
  echo "System log file deleted.\n";
  }
$subfile = substr($filename,0,$pos).".sub";
echo "Batch file = [$subfile].\n";
if ( unlink($subfile) == true ) {
  echo "System batch file deleted.\n";
  }

//
//   Should be able to remove directory if empty 
//
$pos = strrpos($filename,"/");
$dirname = substr($filename,0,$pos);
echo "Current directory = [$dirname]\n";
if ( rmdir($dirname) == true )
  echo "Removed empty directory $dirname\n";

// Finished
return;

?>
