<?php
// Simple PHP script to read the contents
// of a file and return to webpage via AJAX.
//
// Requires filename including full relative path
//

//echo "Using PHP to read file.\n";

// Assign parameters to local variables
$filename = $_POST['filename'];
echo "Filename = [" . $filename . "]\n";

// Open file and return raw contents to user
$fh = fopen($filename,'r') or die("Unable to open file $filename");
while ( !feof($fh)) {
  echo fgets($fh);
  }
fclose($fh);

?>
