<?php
//
//   PHP script to display directory listing
//

//
//   These values must match values in write.php
//   Set parameters defining:
//      base:  base of directory path
//      admin: name of administrative user
//
$base = "data";
$admin = "admin";

//
//   Get subdirectory from AJAX _POST
//
$subdir = $_POST['path'];
if ( $subdir[0] == "/" )
  $subdir = substr($subdir, 1);

//
//   Get username
//
$user = $_SERVER['PHP_AUTH_USER'];

//
//   Build file path
//
$mypath = $base;
if ( "$user" != "$admin")
  $mypath = "$mypath/$user";
if (strlen($subdir) > 2)
  $mypath = "$mypath/$subdir";

//
//   Set allowed extensions to show user
//
$molext = array (".xyz", ".mol");
$txtext = array (".inp", ".out", ".sum");

//
//   Read sorted list of files and directories into array
//
$filelist = scandir($mypath);

//
//   Set values for icon images
//
$iconFolder = "<img src=\"icon_folder.png\" width=\"16\" height=\"16\" />";
$iconMol    = "<img src=\"icon_molecule.png\" width=\"16\" height=\"16\" />";
$iconText   = "<img src=\"icon_textfile.png\" width=\"16\" height=\"16\" />";
$iconDelete = "<img src=\"icon_delete.png\" width=\"16\" height=\"16\" />";

//
// Output list of all directories
//
if (strlen($subdir) > 2) {
  echo "<div class=\"DelColumn\" >&nbsp;</div>";
  echo "<div class=\"DirOrFile\" onclick=\"dirList();\">";
  echo "$iconFolder Home Directory</div>";
  }
for ($i=0; $i < count($filelist); $i++) {
  if (strlen($filelist[$i]) > 2) {
    if ( strpos($filelist[$i], ".") === false ) {
      echo "<div class=\"DelColumn\" >&nbsp;</div>";
      echo "<div class=\"DirOrFile\" onclick=\"dirList('$filelist[$i]');\">";
      echo "$iconFolder $filelist[$i]</div>";
      }
    }
  }

//
// Output list of all files
//
for ($i=0; $i < count($filelist); $i++) {
  if (strlen($filelist[$i]) > 2) {
    // If file is of 'molecule' type, show file
    for ($j=0; $j < count($molext); $j++) {
      if (strpos($filelist[$i],$molext[$j]) > 0) {
        echo "<div class=\"DelColumn\" onclick=\"fillDelWin('$mypath/$filelist[$i]');\">";
        echo "$iconDelete </div> ";
        echo "<div class=\"DirOrFile\" onclick=\"showname('$mypath/$filelist[$i]');\">";
        echo "$iconMol $filelist[$i]</div>";
        }
      }
    // If file is of allowed 'text' type, show file
    for ($j=0; $j < count($txtext); $j++) {
      if (strpos($filelist[$i],$txtext[$j]) > 0) {
        echo "<div class=\"DelColumn\" onclick=\"fillDelWin('$mypath/$filelist[$i]');\">";
        echo "$iconDelete </div> ";
        echo "<div class=\"DirOrFile\" onclick=\"showname('$mypath/$filelist[$i]');\">";
        echo "$iconText $filelist[$i]</div>";
        }
      }
    }
  }

// Finished
return;
?>
